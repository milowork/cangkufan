﻿欢迎使用 Milowork Cangkufan 2.33 版本。
本版本于11月16日发布。

〓〓操作方法〓〓
（菜单中）
·方向键：选择/调整
·Z：返回
·X：确定
（游戏中）
·方向键：移动
·Z：撤销一步
·X/CTRL：拉箱子（Pukoban）
·C+方向键：移动视野
·V/ESC：暂停
Z与X键的作用可以在设置中调换。
另外，将 XSB 文件放入 XSB 文件夹可以使本程序加载；也可以在 XSB 界面按 Ctrl+V 从剪贴板加载。

〓〓2.33更新内容〓〓
·XSB 增强：
  ·支持 SOK、SKB 格式
  ·XSB 选择界面左右箭头提示
  ·支持在 XSB 界面从剪贴板加载，解决后，解法和关卡一同返回剪贴板（仅限嵌入版 Python）
  ·XSB 选择界面自然排序功能（仅限嵌入版 Python）
·初步支持 pygame 2（此时 Z-X 键互换功能失效）
·轻微的外观变化
·BUG 修复：
  ·无法识别单行分隔的多关卡 XSB
  ·在使用嵌入版 Python 时，报错无法弹窗，直接闪退
  ·语言选择时的字体显示错位
  ·音量无法归零
  ·在无声卡情况下闪退

〓〓2.30更新内容〓〓
·启动加速
·添加标题 BGM
·第0关音乐室：在最底行行走以播放相应 BGM
·中文、日文可显示汉字、假名；法语被取消
·地砖上的文本由 SOKO 改为 CKF2；角色图像细节调整
·ESC 键可暂停，CTRL 键可拉箱子
·现在长按方向键可以连续的推了
·控制台被隐藏；错误信息弹出对话框而不是在控制台输出
·XSB 增强：
  ·XSB 选择界面改进
  ·支持 TXT、QSB 格式
  ·自动拆分多关卡 XSB
  ·支持 UTF-8、GBK 编码
  ·现在每次打开同名 XSB 时的图块、人物、背景、BGM 均相同
  ·识别非法 XSB 文件中的错误
·BUG 修复：
  ·在旧版本 Python 下因 GIL 问题而闪退
  ·加载大地图时的递归溢出
  ·XSB 格式下无法横向滚动

〓〓2.20更新内容〓〓
·自带 Python 嵌入版，Vista 以上系统无需安装 Python 即可运行。
·新的角色：初音未来、博丽灵梦、雾雨魔理沙。
·支持载入 XSB 格式地图（但暂不支持多张地图存入同一 XSB 文件）。
·可记忆关卡的最佳成绩，并保存解法。
·支持变种：拉箱子（Pukoban），对应第155~164关。
·在线版本更新检查。

〓〓运行方法〓〓
·Vista~Win10（带 Python 版本）：直接运行 play.cmd 即可。
·XP~2003：
  ·到 https://milo.work/w/pygame 下载并安装 pygame 一键安装包。
  ·之后运行 play.cmd。请注意，此时本程序部分功能不可用。
·Linux/macOS（尚未测试）：手动安装 Python 和 pygame，之后运行 main.py。
·2000/9X/Android/iOS 等：暂不支持。
·从旧版本升级方法：直接覆盖即可。

〓〓版权信息〓〓
本程序的版权归 Milowork 所有，依照 MCC-320 协议发布。
本程序含有来自 Vocaloid、东方 Project 的角色，请遵循其相应的版权许可。
内置关卡第000~064源自文曲星电子词典内置的64个关卡。
内置关卡第066~154源自阿尔伯塔大学发布的90个关卡。
内置关卡第155~164为 Pukoban 关卡。
背景音乐信息详见 bgm/info.txt。
音效均为从爱给网下载。
本游戏借鉴自 Sokoban，版权归 Falcon 所有。
本游戏借鉴自 Pukoban，版权归 Dries De Clercq 所有。
本程序如有侵权内容，请与 Milowork 联系，我们将于下个版本移除该内容，并停止旧版本的发布。

〓〓联系方式〓〓
发布页面：https://milo.work/w/ckf
官网：https://milo.work/
邮箱：milo.work@qq.com
QQ：3145471450
Overseas E-mail: milowork2@yahoo.co.jp

〓〓参考链接〓〓
阿尔伯塔大学发布的90个关卡：https://webdocs.cs.ualberta.ca/~games/Sokoban/Mazes/Screens/