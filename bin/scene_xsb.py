################################
##    CANGKUFAN SCENE_XSB     ##
##   COPYLEFT 2019 MILOWORK   ##
################################

# 导入
import pygame, os
import gv
try: from natsort import natsorted
except ImportError: natsorted=sorted # 替代方案
try: from pyperclip import paste
except ImportError: paste=lambda:''
from func import *
import scene_title, scene_main
pagelen=10
def setup():
    global starttick, endtick, flist, page, cur, printst
    pygame.display.set_caption("Cangkufan %s"%gv.VERSION)
    starttick=gettick(); endtick=0
    flist=getlist()
    if gv.LEVEL in flist:
        i=flist.index(gv.LEVEL); page=i//pagelen; cur=i%pagelen
    else: page=0; cur=0
    playbgm("5"); printst=0

def getlist():
    l=[]
    for fn in os.listdir(gv.MYPATH+"xsb/"):
        (f, ext)=os.path.splitext(fn)
        if ext.lower() in ('.xsb','.txt','.skb','.sok','.qsb','.xsbl'): l.append(fn)
    l=natsorted(l)
    if not l: l=['-']
    l.extend('-'*-(len(l)%-pagelen)); return l

def loop():
    global starttick, endtick, nextscene, flist, page, cur, printst
    
    gv.SCREEN.fill(pygame.Color(0,0,0,0))
    tick=gettick()
    if endtick: pass
    elif gv.KEY1[pygame.K_x]:
        if cur==pagelen:
            nextscene=scene_title; playse('ok')
            endtick=tick; fadebgm()
        else:
            fn=flist[page*pagelen+cur]
            if fn!='-':
                gv.SETLEVEL(fn)
                nextscene=scene_main; playse('ok')
                endtick=tick; fadebgm()
            else: playse('void')
    elif gv.KEY1[pygame.K_z]:
        nextscene=scene_title; playse('back')
        endtick=pygame.time.get_ticks()
        fadebgm()
    elif gv.KEY1[pygame.K_DOWN] and cur<pagelen:
        playse('select'); cur+=1; printst=tick
    elif gv.KEY1[pygame.K_UP]   and cur>0:
        playse('select'); cur-=1; printst=tick
    elif gv.KEY1[pygame.K_RIGHT]and page<len(flist)//pagelen-1:
        playse('select'); page+=1; printst=tick
    elif gv.KEY1[pygame.K_LEFT] and page>0:
        playse('select'); page-=1; printst=tick
    elif gv.KEY1[pygame.K_v] and gv.KEY[pygame.K_LCTRL] and paste():
        gv.SETLEVEL('\n'+paste())
        nextscene=scene_main; playse('ok')
        endtick=tick; fadebgm()
    
    drawbg(9)
    lr=True if gv.XSIZE<=480 else False
    
    printg("LOAD XSB",(gv.XSIZE/2,gv.YSIZE/(pagelen+3)),2,1,a=4,t=1)
    if page>                    0: printg("←", (         16, gv.YSIZE/2), 2, a=4)
    if page<len(flist)//pagelen-1: printg("→", (gv.XSIZE-16, gv.YSIZE/2), 2, a=4)

    for i in range(page*pagelen,min((page+1)*pagelen,len(flist))):
        i0=i-page*pagelen
        printg(os.path.splitext(flist[i])[0].upper(), (gv.XSIZE/2, gv.YSIZE/(pagelen+3)*(i0+2)),\
               1 if cur==i0 else 0, 3 if cur==i0 else 0, printst, a=4, t=1)

    printg("TITLE①"if lr else "TITLE",(gv.XSIZE/2,gv.YSIZE/(pagelen+3)*(pagelen+2)),1 if cur==pagelen else 0,3 if cur==pagelen else 0,printst,a=4,t=1)
    
    fadein(starttick)
    if(endtick):
        fadeout(endtick)
        if gettick()>endtick+500: gv.SETSCENE(nextscene)
