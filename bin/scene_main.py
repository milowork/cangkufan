################################
##    CANGKUFAN SCENE_MAIN    ##
##   COPYLEFT 2019 MILOWORK   ##
################################

# 导入
import sys, os, csv, pygame
import gv, scene_pause, scene_xsb, scene_title
from func import *
from binascii import crc32

# 定义变量
global starttick, endtick, tick, prevtick
global map_, charxy, boxxy, moving, movbox
global xoff, yoff, xscr, yscr
global dir, kdir, walkse, step, history
global mapsur, tileset

# XYTOXY
def xytoxy(xy,d,o):
    x=0; y=0
    if o!=0:
        if   d==0: y=-o # 下
        elif d==1: x= o # 左
        elif d==2: x=-o # 右
        elif d==3: y= o # 上
    return xy[0]*gv.SIZE+xoff+x,xy[1]*gv.SIZE+yoff+y

# 检测箱子就位与否
def cheque():
    global endtick, map_, boxxy
    for e in boxxy:
        if map_[e[1]][e[0]]!=1: return
    endtick=pygame.time.get_ticks(); playse('complete') # 淡出，下一关
    if isinstance(gv.LEVEL, str) or getbgm(gv.LEVEL)!=getbgm(gv.LEVEL+1):fadebgm()

def setup(setup_=True):
    if not setup_: return
    global map_, charxy, boxxy, best # 读取地图
    map_=[]; charxy=(0,0); boxxy=[]
    pygame.display.set_caption("Cangkufan %s - %s"%(gv.VERSION,gettitle(True)))
    if isinstance(gv.LEVEL,str): # XSB 格式
        if gv.LEVEL[0]!='\n':
            fn=gv.MYPATH+"xsb/"+gv.LEVEL
            try: # 试着用 UTF-8 解码
                with open(fn, encoding="utf-8") as f:
                    fs=f.read()
                    if   fs[:2]=='GB': raise UnicodeError
                    elif fs[0] =='\uFEFF': fs=fs[1:]
            except FileNotFoundError:
                msgbox(tran("File %s not found.")%fn); gv.SETSCENE(scene_xsb); return
            except UnicodeError: # 备选编码：GBK
                with open(fn, encoding="gbk") as f: fs=f.read()
            if '\n\n' in fs: # 多关卡 XSB 拆分
                import os, re
                levels=[st for st in fs.split('\n\n') if st.strip()]; i=0
                if len(levels)>1:
                    for le in levels:
                        mat=re.search('Title:.*',le,re.I); name=mat.group()[6:].strip() if mat else str(i+1)
                        name=re.sub(r'[/\\:*?"<>|]','-',name) # 替换文件名非法字符
                        with open(gv.MYPATH+"xsb/"+os.path.splitext(gv.LEVEL)[0]+'-'+name+".xsbl",'w',encoding='utf-8') as f2: f2.write(le); i+=1
                    os.rename(fn, gv.MYPATH+"xsb/"+gv.LEVEL+".bak") # msgbox(tran("Multilevel XSB is decomposed."), True)
                    gv.SETSCENE(scene_xsb); return
        else: fs=gv.LEVEL
        y=0
        for row_str in fs.splitlines(): # XSB 格式解码开始
            row=[]; x=0; dic={'_':-2,' ':-2,'-':-2,'.':-1,'$': 2,'*':3,'#':4,'@':8,'+':9}
            if (not row_str.strip()) or row_str[0] not in dic: continue # 非内容行
            for ele in row_str:
                a=dic.get(ele, None)
                if a is None:
                    msgbox(tran("Line %d containing illegal char.")%(y+1)); gv.SETSCENE(scene_xsb); return
                if a==2 or a==3: a-=4 ; boxxy.append((x, y))  # 箱子
                if a==8 or a==9: a-=10; charxy=(x, y)  # 主角
                row.append(a); x+=1
            map_.append(row); y+=1
    else: # 专有格式
        fn=gv.MYPATH+"map/%d.map"%gv.LEVEL
        with open(fn, encoding="utf-8") as f:
            mapreader=csv.reader(f); y=0
            for row_str in mapreader:
                row=[]; x=0
                for ele in row_str:
                    if ele=="-": ele="-1"
                    elif ele==" ": ele="0"
                    elif ele=="#": ele="4"
                    a=int(ele)
                    if a==2 or a==3: a-=2; boxxy.append((x, y))  # 箱子
                    if a==8 or a==9: a-=8; charxy=(x, y)  # 主角
                    row.append(a); x+=1
                map_.append(row); y+=1  
    try:
        assert str(gv.LEVEL)[0]!='\n' # 是否从剪贴板加载
        with open(gv.MYPATH+"sol/"+str(gv.LEVEL)+".txt",encoding="utf-8") as f: best=f.read() # 尝试读取答案
    except FileNotFoundError:
        if isinstance(gv.LEVEL, str) and not gv.LEVEL.isnumeric():
            try: # 这里是为了兼容 2.20 版本
                import os
                with open(gv.MYPATH+"sol/"+os.path.splitext(str(gv.LEVEL))[0]+".txt",encoding="utf-8") as f: best=f.read()
            except FileNotFoundError: best=None
        else: best=None
    except AssertionError: best=None
    if len(map_)<4 or len(map_[0])<4:  msgbox(tran("A map must have at least 4*4 of size."));  gv.SETSCENE(scene_xsb); return
    for y in range(len(map_)):
        if len(map_[y])!=len(map_[0]): msgbox(tran("Length of line %d is incorrect.")%(y+1)); gv.SETSCENE(scene_xsb); return
    floodfill(charxy[0], charxy[1])
    global mapsur, tileset # 绘制地图「表面」
    tileset=gettileset(gv.LEVEL)
    mapsur=pygame.Surface((len(map_[0])*gv.SIZE,len(map_)*gv.SIZE))
    mapsur.fill        ((127,127,127))
    mapsur.set_colorkey((127,127,127))
    for y in range(len(map_)):
        for x in range(len(map_[0])):
            t=map_[y][x]
            if   t  < 0: continue
            elif t&5==0: t=0 if tileset<4 else 4
            elif t&5==1: t=1 if tileset<4 else 5
            elif t  ==4: t=8+tileset
            mapsur.blit(gv.TILE[t],(x*gv.SIZE,y*gv.SIZE))
    
    # 初始化变量
    global starttick, endtick, tick, prevtick
    starttick=gettick(); endtick=0; tick=starttick; prevtick=starttick
    global xoff, yoff, xscr, yscr, moving, movbox
    global dir, kdir, walkse, step, history
    xscr=max((len(map_[0])+1)*gv.SIZE-gv.XSIZE,0) # 滚动幅度
    yscr=max((len(map_   )+1)*gv.SIZE-gv.YSIZE,0)
    xoff=0; yoff=0; moving=0; movbox=()
    dir=0;  kdir=-1; walkse=0; step=''; history=[]
    playbgm(getbgm(gv.LEVEL)) # BGM

def floodfill(x, y): # 填充算法（非递归版）
    global map_
    queue=[(x,y)]
    for p in queue:
        if map_[p[1]][p[0]]<0:
            map_[p[1]][p[0]]+=2
            if y>             0: queue.append((p[0],p[1]-1))
            if y<len(map_   )-1: queue.append((p[0],p[1]+1))
            if x>             0: queue.append((p[0]-1,p[1]))
            if x<len(map_[0])-1: queue.append((p[0]+1,p[1]))

def getbgm(level):      # 获取某关的 BGM; 之所以做成函数是为了判断是否淡出
    if isinstance(level, str): return str(crc32(level.encode())%6+1)
    if   level>gv.MAXLEVEL: level=gv.MAXLEVEL
    if   level<=  0:a=-1
    elif level<= 64:a=(level-  1)//2%4+1   # 每2关更换，12341234
    elif level<=100:a=(level- 65)//2%9+1   # 每2关更换，123412345
    elif level<=154:a=(level-101)//2%9+1   # 每2关更换，123412345
    else           :a=10
    return str(a if a<=4 else a-4)

def gettileset(level):  # 获取某关的 TILESET
    if isinstance(level, str):
        if os.path.splitext(level)[1].lower()=='.qsb': return 7
        else: return crc32(level.encode())%7
    if   level>gv.MAXLEVEL: level=gv.MAXLEVEL
    if   level<=  0:return 3
    elif level<= 64:return (level-  1)//8%2             # 每8关更换，0   1
    elif level<=100:return (level- 65)%18//8            # 每2关更换，000011112
    elif level<=154:return (level-101)%18//8+4          # 每2关更换，444455556
    else           :return 7

def getbg(level):  # 获取某关的 TILESET
    if isinstance(level, str): return crc32(level.encode())%11+1
    elif level<=  0:return 0
    elif level<= 16:return 1
    elif level<= 32:return 2
    elif level<= 48:return 3
    elif level<= 64:return 4
    elif level<= 82:return 5
    elif level<=100:return 6
    elif level<=118:return 7
    elif level<=136:return 8
    elif level<=154:return 9
    elif level<=164:return 10
    else           :return 11

def getchar(level):
    if isinstance(level, str): return crc32(level.encode())%5
    elif level<=  0: return 5
    elif level<=100: return (level-1)%2
    elif level<=154: return (level-1)%2+2
    else           : return 4

def gettitle(full=False):
    if   isinstance(gv.LEVEL, str): return gv.LEVEL if full else os.path.splitext(gv.LEVEL)[0]
    elif gv.LEVEL==0: return tran("MUSIC ROOM")
    else: return tran("LEVEL")+' '+str(gv.LEVEL)
    
def loop(pause=0):
    global starttick, endtick, tick, prevtick, tileset
    global map_, charxy, boxxy, moving, movbox
    global xoff, yoff, xscr, yscr, mapsur
    global dir, kdir, walkse
    global step, history, best
    if not map_: return
    prevtick=tick; tick=gettick() # 获取时间
    kdir=-1
    if   gv.KEY [pygame.K_c]    : kdir=-4
    elif gv.KEY[pygame.K_DOWN]  : kdir=0
    elif gv.KEY[pygame.K_LEFT]  : kdir=1
    elif gv.KEY[pygame.K_RIGHT] : kdir=2
    elif gv.KEY[pygame.K_UP]    : kdir=3
    elif gv.KEY1[pygame.K_z]    : kdir=-2
    elif gv.KEY1[pygame.K_v] or gv.KEY1[pygame.K_ESCAPE]: kdir=-3
    if tileset==7 and (gv.KEY[pygame.K_x] or gv.KEY[pygame.K_LCTRL]) and -1<=kdir<4: kdir+=5
    a=charxy; b=(0,0); c=(0,0)
    if moving <= tick: moving=tick; movbox=(); dir%=4 # 移动已经完成
    if moving <= tick and not pause:
        if   kdir==0: b=(a[0],a[1]+1); c=(a[0],a[1]+2) # 下
        elif kdir==1: b=(a[0]-1,a[1]); c=(a[0]-2,a[1]) # 左
        elif kdir==2: b=(a[0]+1,a[1]); c=(a[0]+2,a[1]) # 右
        elif kdir==3: b=(a[0],a[1]-1); c=(a[0],a[1]-2) # 上
        if 4>kdir>=0 and map_[b[1]][b[0]]<2:
            i=0
            for ele in boxxy: # 遍历箱子
                if ele == b:
                    if (gv.KEY0[pygame.K_DOWN] or gv.KEY0[pygame.K_UP   ] or \
                        gv.KEY0[pygame.K_LEFT] or gv.KEY0[pygame.K_RIGHT]) and \
                       (history and len(history[-1])<=1): i=-1; break # 按着
                    if map_[c[1]][c[0]]>=2 or c in boxxy: i=-2; break # 箱子后面是墙/箱子
                    history.append(((charxy,b),(boxxy[i],c))); boxxy[i]=c; charxy=b # 推箱子
                    dir=kdir+4; moving=tick+256; movbox=(i,); i=-4; step+=('D','L','R','U')[dir%4]
                    cheque(); playse('select' if map_[c[1]][c[0]]%2==1 else 'push'); break
                i+=1 # 没有箱子
            if i>=0: 
                if gv.LEVEL==0 and b[1]==3: playbgm(str(b[0]-2)) # 音乐室
                history.append(((charxy,b),)); charxy=b # 走一步
                dir=kdir; moving = tick+128; step+=('d','l','r','u')[dir]
                if tick-walkse>=200: playse('walk'); walkse=tick
        elif kdir==-2: # 撤销
            print(history)
            if step:
                a=history.pop(); charxy=a[0][0]; step=step[:-1]; playse('undo')
                for b in a[1:]: boxxy[boxxy.index(b[1])]=b[0]
            else: playse('void')
        elif kdir==-3: # 暂停
            gv.SETSCENE(scene_pause); playse('pause')
        elif kdir>=5: # 拉箱子
            if   kdir==8: b=(a[0], a[1]+1); z=(a[0], a[1]-1)  # 面朝下，移动向上
            elif kdir==7: b=(a[0]-1, a[1]); z=(a[0]+1, a[1])  # 面朝左，移动向右
            elif kdir==6: b=(a[0]+1, a[1]); z=(a[0]-1, a[1])  # 面朝右，移动向左
            elif kdir==5: b=(a[0], a[1]-1); z=(a[0], a[1]+1)  # 面朝上，移动向下
            else        : z=None
            if z and (map_[z[1]][z[0]]<2) and (z not in boxxy):
                i=0
                for ele in boxxy:
                    if ele == b:
                        history.append(((charxy,z),(boxxy[i],a))); boxxy[i]=a; charxy=z
                        dir=16-kdir; moving=tick+256; movbox=(i,)
                        step+=('s','y','z','x')[dir%4]; cheque()
                        playse('select' if map_[a[1]][a[0]]%2==1 else 'push'); break
                    i+=1
        if kdir==4 and not (gv.KEY0[pygame.K_x] or gv.KEY0[pygame.K_LCTRL]): playse('grab')
    if kdir==-4:
        xoff+=(gv.KEY[pygame.K_LEFT]-gv.KEY[pygame.K_RIGHT])*(tick-prevtick)
        yoff+=(gv.KEY[pygame.K_UP  ]-gv.KEY[pygame.K_DOWN ])*(tick-prevtick)
    else:
        xoff=0; yoff=0
        (charx, chary)=xytoxy(charxy*32,dir%4,(moving-tick)/(4,8,-8)[dir//4]) # 根据角色位置计算 XOFF / YOFF
        xoff=round((gv.XSIZE/2)-len(map_[0])*(gv.SIZE/2) - ((charx/gv.SIZE-1)/(len(map_[0])-3)-0.5)*xscr)
        yoff=round((gv.YSIZE/2)-len(map_   )*(gv.SIZE/2) - ((chary/gv.SIZE-1)/(len(map_   )-3)-0.5)*yscr)
    drawbg(getbg(gv.LEVEL)) # 清屏
    gv.SCREEN.blit(mapsur,(xoff,yoff)) # 显示地图
    for i in range(len(boxxy)): # 显示箱子
        tileno=3 if map_[boxxy[i][1]][boxxy[i][0]]==1 and i not in movbox else 2
        if tileset>=4: tileno+=4
        gv.SCREEN.blit(gv.TILE[tileno],xytoxy(boxxy[i],dir%4,((moving-tick)/8)*(1 if dir<8 else -1) if i in movbox else 0))
    gv.SCREEN.blit(gv.CHAR[(dir%4+(kdir>=4 or dir>=4)*4)+(8*getchar(gv.LEVEL))],\
                   xytoxy(charxy*32,dir%4,(moving-tick)/(4,8,-8)[dir//4])) # 显示主角
    printg(str(len(step)),(0,0)); best and printg(str(len(best)),(0,gv.SIZE),2) # 显示步数
    dispmsg(gettitle(), starttick) if tick-starttick<3000 and str(gv.LEVEL)[0]!='\n' else None # 显示关数
    fadein(starttick) # 淡入
    if endtick: # 淡出与下一关
        fadeout(endtick)
        if gettick()>endtick+500:
            if not best or len(step)<len(best):
                if str(gv.LEVEL)[0]!='\n':
                    try:
                        with open(gv.MYPATH+"sol/"+str(gv.LEVEL)+".txt", 'w', encoding="utf-8") as f: f.write(step)
                    except Exception: pass
                else:
                    import pyperclip
                    pyperclip.copy('Solution: '+step+gv.LEVEL)
            if isinstance(gv.LEVEL, str): gv.SETSCENE(scene_xsb)
            else: gv.SETLEVEL(gv.LEVEL+1); setup()
