################################
## CANGKUFAN GLOBAL VARIABLES ##
##   COPYLEFT 2019 MILOWORK   ##
################################

# 导入
import pygame
import sys, os, csv

# 变量一览
global MYPATH, IMGPATH, MAPPATH, MAXLEVEL
global SIZE, SIZE2, XSIZE, YSIZE, SCREENSIZE
global CLOCK, SCENE, PREVSCENE
global KEY, KEY0, KEY1, LEVEL, BGM, ZXXCH
global LANGUAGE, BGMVOL, SEVOL
global RESOLUTION, SCREEN
global TILE, CHAR, FONT, XFONT, XFONTCACHE, UI, BG, SE, DICT
global VERSION, NEWEST

# 初始化
def SETUP():
    # 更新检查与 GIL 初始化
    from threading import Thread 
    t=Thread(target=lambda:None); t.start(); t.join()
    t=Thread(target=CHEQUEVER   ); t.start()
    
    # PYGAME初始化
    pygame.mixer.pre_init(frequency=44100, size=-16, channels=2, buffer=1024)
    pygame.init()
    
    # 全局常量的初始化
    global MYPATH, IMGPATH, MAPPATH # 程序路径 / 图像路径 / 地图路径
    global SIZE, SIZE2              # 图块尺寸 / 图块尺寸元组
    global MAXLEVEL                 # 关卡数
    MYPATH=os.path.split(os.path.dirname(__file__))[0]+"/"
    IMGPATH=MYPATH+"img/"
    MAPPATH=MYPATH+"map/"
    SIZE=32; SIZE2=(SIZE,SIZE)
    MAXLEVEL=164
    
    # 全局变量的初始化
    global CLOCK
    global SCENE, PREVSCENE
    global KEY, KEY0, KEY1
    global LEVEL, BGM
    CLOCK=pygame.time.Clock()
    PREVSCENE=None; SCENE=None
    KEY=pygame.key.get_pressed(); KEY0=[]; KEY1=[]
    LEVEL=1; BGM="" # 第0关存在，但仅用于测试
    
    # 加载设置
    global LANGUAGE, BGMVOL, SEVOL, RESOLUTION, ZXXCH
    try:
        with open(MYPATH+"config.ini",encoding="utf-8") as f:
            arr=csv.reader(f).__next__()
            BGMVOL=int(arr[0]); SEVOL=int(arr[1]); LANGUAGE=int(arr[2])
            RESOLUTION=int(arr[3])
            ZXXCH=int(arr[4])
    except Exception:
        BGMVOL=8; SEVOL=8; LANGUAGE=0; RESOLUTION=8000480; ZXXCH=0
    
    # 显示初始化
    global VERSION, NEWEST
    VERSION='2.33'; NEWEST=None
    pygame.display.set_caption("Cangkufan %s"%VERSION)
    global SCREEN, XSIZE, YSIZE, SCREENSIZE
    if   RESOLUTION>  10000: # 该分辨率窗口
        SCREEN    =pygame.display.set_mode((  RESOLUTION //10000,  RESOLUTION %10000)                  )
    elif RESOLUTION>=-10000: # 自动分辨率全屏(可能卡顿)
        SCREEN    =pygame.display.set_mode( (                  0,                  0),pygame.FULLSCREEN)
    else                   : # 该分辨率全屏(可能失败)
        try:SCREEN=pygame.display.set_mode(((-RESOLUTION)//10000,(-RESOLUTION)%10000),pygame.FULLSCREEN)
        except pygame.error: # 失败后自动分辨率全屏
            SCREEN=pygame.display.set_mode( (                  0,                  0),pygame.FULLSCREEN)
    dispinfo=pygame.display.Info()
    XSIZE=dispinfo.current_w; YSIZE=dispinfo.current_h
    SCREENSIZE=(XSIZE,YSIZE)
    
    # 全局图块资源加载
    global TILE, CHAR, FONT
    global BG, UI, DICT
    TILE = []; CHAR = []; FONT = []
    UI   = {}; BG   = []; DICT = {}
    
    charimg=pygame.image.load(IMGPATH+"tile.png").convert() 
    charimg.set_colorkey((127,127,127))
    for i in range(0,48):
        CHAR.append(charimg.subsurface(pygame.Rect((i%16*SIZE,i//16*SIZE),SIZE2)).convert())
    for i in range(0,16):
        TILE.append(charimg.subsurface(pygame.Rect((i   *SIZE,3    *SIZE),SIZE2)).convert())
    
    fontimg=pygame.image.load(IMGPATH+"font.png").convert()
    fontimg.set_colorkey((0,0,0))
    for y in range(0,32):
        for x in range(0,16):
            FONT.append(fontimg.subsurface(pygame.Rect((x*SIZE,y*SIZE),SIZE2)).convert())
    
    UI.clear(); uilist={'pause','title','title2'}
    for k in uilist: UI[k]=(pygame.image.load(IMGPATH+"ui_%s.png"%k).convert())
    
    bgimg=pygame.image.load(IMGPATH+"bg.png").convert()
    for x in range(0,12):
        BG.append(bgimg.subsurface(pygame.Rect((x*256,0),(256,384))).convert())
    
    # 音频初始化、音频资源加载
    global SE
    SE={}
    try:
        pygame.mixer.set_num_channels(16)
        LOADSE()
    except: SE=None # 没有声卡
    SETVOL(bgm=BGMVOL,se=SEVOL) # 音量的初始化
    SETLANG(LANGUAGE);  # 语言资源的初始化

def SETSCENE(sc, *arg):
    global SCENE, PREVSCENE
    if sc==None: sys.exit()
    PREVSCENE=SCENE; SCENE=sc
    sc.setup(*arg)

def REFRESHKEY():
    global KEY, KEY0, KEY1, ZXXCH
    pygame.event.pump(); KEY0=KEY
    KEY=pygame.key.get_pressed(); lk=list(KEY)
    if pygame.version.vernum[0]==2:
        lk0=list(KEY0); KEY1=type(KEY)([lk[i] and not lk0[i] for i in range(len(KEY))])
    else:
        if ZXXCH: lk[pygame.K_z], lk[pygame.K_x]=lk[pygame.K_x], lk[pygame.K_z]; KEY=type(KEY0)(lk)
        KEY1=[KEY[i] and not KEY0[i] for i in range(len(KEY))]

def SETLEVEL(l):
    global LEVEL, MAXLEVEL
    if isinstance(l, str):  LEVEL=l
    elif l<=0:              LEVEL=0
    elif l<=MAXLEVEL:       LEVEL=l
    else:                   LEVEL=MAXLEVEL
        
def SETLANG(l):
    global LANGUAGE, DICT, XFONT, XFONTCACHE
    DICT.clear()
    languages=3
    if   0<l<languages: LANGUAGE=l
    elif l<0:           LANGUAGE=languages-1
    else:               LANGUAGE=0
    if LANGUAGE: # 这个 encoding="utf-8" 非常重要，如果没有的的话会导致乱码(显示■)或闪退
        f=open(MYPATH+"lan/"+str(LANGUAGE)+".lan",encoding="utf-8")
        lan = csv.reader(f)
        for row in lan:
            if len(row) and row[0][0]!='#': DICT[row[0]]=row[1].strip()     # 跳过空行和注释
        f.close()
    if LANGUAGE==2: XFONT=pygame.font.Font(MYPATH+"lan/Chibisuke.ttf", 32)
    else          : XFONT=pygame.font.Font(MYPATH+"lan/zkklt.ttf"    , 32)
    XFONTCACHE={}

def SETVOL(bgm=None,se=None):
    global BGMVOL, SEVOL, SE
    if bgm is not None:
        if 0<=bgm<=10:
            BGMVOL=bgm
            if SE==None: return 
            pygame.mixer.music.set_volume(bgm/10)
    if  se is not None:
        if  0<=se<=10:
            SEVOL =se
            if SE==None: return 
            for ele in SE: SE[ele].set_volume(se/10)
    
def LOADSE():
    global SE
    SE.clear()
    sepath = MYPATH+"se/"
    for fn in os.listdir(sepath):
        (f,ext)=os.path.splitext(fn)
        if ext==".wav" or ext==".ogg":
            SE[f]=pygame.mixer.Sound(sepath+fn)
            
def SETBGM(bgm): # 以下为简单 SETTER
    global BGM; BGM=bgm
def SETRESOLUTION(r):
    global RESOLUTION; RESOLUTION=r

def XFONTRENDER(c,co):
    global XFONTCACHE
    if (c,co) in XFONTCACHE: return XFONTCACHE[(c,co)]
    else:
        fo=-6 if LANGUAGE==2 else 0
        su=pygame.Surface(SIZE2, pygame.SRCALPHA)
        fr=XFONT.render(chr(c),1,(255,255,255))
        su.blits(( (fr,(-1,fo)),(fr,(0,fo)),(fr,(1,fo)),(su,(0,-1)),(su,(0,1)) ))
        s2=FONT[co*128].convert_alpha()
        s2.blit(fr,(0,fo),special_flags=pygame.BLEND_RGBA_MULT); su.blit(s2,(0,0))
        XFONTCACHE[(c,co)]=su; return su

def CHEQUEVER():
    global VERSION, NEWEST
    try:
        import socket, urllib.request
        socket.setdefaulttimeout(5) # 不无限等待连接更新检查服务器
        t=urllib.request.urlopen('http://milo.work/v/sokoban2.txt').read().decode('utf-8','ignore').splitlines()
        if t[0]>VERSION: NEWEST=t[0]
    except Exception: return
