################################
##   CANGKUFAN  SCENE_PAUSE   ##
##   COPYLEFT 2019 MILOWORK   ##
################################

# 导入
import pygame
import time, sys
import gv
from func import *
import scene_select, scene_xsb, scene_main

global endtick, cur, printst

def setup():
    global endtick, cur, printst
    endtick=0; cur=0; printst=gettick(); pausebgm()

def loop():
    global endtick, cur, printst
    tick=gettick()
    if endtick: pass
    elif   gv.KEY1[pygame.K_x]:
        playse('undo' if cur==1 else 'ok')
        if cur==0: pausebgm(); gv.SETSCENE(scene_main, False)
        else: endtick=pygame.time.get_ticks()
    elif gv.KEY1[pygame.K_v] or gv.KEY1[pygame.K_z] or gv.KEY1[pygame.K_ESCAPE]:
        playse('back'); pausebgm(); gv.SETSCENE(scene_main, False)
    elif gv.KEY1[pygame.K_DOWN] and cur<2 and not endtick: playse('select'); cur+=1; printst=tick
    elif gv.KEY1[pygame.K_UP]   and cur>0 and not endtick: playse('select'); cur-=1; printst=tick
    gv.SCREEN.blit(gv.UI["pause"],(gv.XSIZE/2-200,gv.YSIZE/2-120))
    printg("CONTINUE",  (gv.XSIZE/2,gv.YSIZE/2-40),1 if cur==0 else 0,3 if cur==0 else 0,printst,a=4,t=1)
    printg("RETRY",     (gv.XSIZE/2,gv.YSIZE/2   ),1 if cur==1 else 0,3 if cur==1 else 0,printst,a=4,t=1)
    printg("SELECT",    (gv.XSIZE/2,gv.YSIZE/2+40),1 if cur==2 else 0,3 if cur==2 else 0,printst,a=4,t=1)
    gv.SCREEN.blit(gv.CHAR[16+2],(gv.XSIZE/2-160-gv.SIZE/2,gv.YSIZE/2-40+cur*40-gv.SIZE/2))
    gv.SCREEN.blit(gv.CHAR[24+1],(gv.XSIZE/2+160-gv.SIZE/2,gv.YSIZE/2-40+cur*40-gv.SIZE/2))
    
    if endtick:
        fadeout(endtick)
        if tick>endtick+500:
            if cur==1: gv.SETSCENE(scene_main, True)
            else:
                pygame.display.set_caption("Cangkufan %s"%gv.VERSION)
                if isinstance(gv.LEVEL,str): gv.SETSCENE(scene_xsb)
                else: gv.SETSCENE(scene_select)
    # 这几行是为了节约 CPU 资源和电量，当 2 秒未按键时，以每秒 10 次的频率检测按键
    elif tick-printst>2000:
        while sum(pygame.key.get_pressed())==0:
            gv.CLOCK.tick(10) # 维持在 10 FPS
            for event in pygame.event.get():
                if event.type == pygame.QUIT: sys.exit()
