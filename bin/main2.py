################################
##       CANGKUFAN 2.30       ##
##   COPYLEFT 2019 MILOWORK   ##
################################

import sys
import pygame, gv, scene_title

def setup():
    gv.SETUP(); gv.SETSCENE(scene_title, 1) # 初始化场景
    while 1: loop()

def loop():
    gv.REFRESHKEY()                     # 检测键盘
    gv.SCENE.loop()                # 执行每帧循环
    pygame.display.update() # pygame.display.flip()         # 刷屏
    for event in pygame.event.get():                        # 退出?
        if event.type == pygame.QUIT: sys.exit()
    gv.CLOCK.tick(60)                   # 维持在 60 FPS 以下
