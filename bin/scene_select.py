################################
##   CANGKUFAN SCENE_SELECT   ##
##   COPYLEFT 2019 MILOWORK   ##
################################

# 导入
import pygame, sys
import gv
from func import *
import scene_title, scene_main

global starttick, endtick, nextscene
global cur, printst # 光标, PRINTC 开始时刻

def setup():
    global starttick, endtick
    global cur, printst
    starttick=gettick(); endtick=0
    cur=2; printst=0; playbgm("0")
    if isinstance(gv.LEVEL, str): gv.SETLEVEL(1)

def loop():
    global starttick, endtick, nextscene
    global cur, printst

    maxlevel=gv.MAXLEVEL
    if endtick: pass
    elif gv.KEY1[pygame.K_x]:
        if cur==3:
            nextscene=scene_title
        else:
            nextscene=scene_main
        endtick=pygame.time.get_ticks()
        playse('ok'); fadebgm()
    elif gv.KEY1[pygame.K_z]: nextscene=scene_title; endtick=gettick(); playse('back'); fadebgm()
    elif gv.KEY1[pygame.K_RIGHT]  and cur<3 : cur+=1; printst=gettick(); playse('select')
    elif gv.KEY1[pygame.K_LEFT]   and cur>0 : cur-=1; printst=gettick(); playse('select')
    elif gv.KEY1[pygame.K_UP]     and cur<=2:
        prevlevel=gv.LEVEL
        if   cur==0 and maxlevel-gv.LEVEL>=  1:gv.SETLEVEL(gv.LEVEL+100)
        elif cur==1 and maxlevel-gv.LEVEL>=  1:gv.SETLEVEL(gv.LEVEL+ 10)
        elif cur==2 and maxlevel-gv.LEVEL>=  1:gv.SETLEVEL(gv.LEVEL+  1)
        playse('select' if prevlevel!=gv.LEVEL else 'void')
    elif gv.KEY1[pygame.K_DOWN]   and cur<=2:
        prevlevel=gv.LEVEL
        if   cur==0 and          gv.LEVEL>=100:gv.SETLEVEL(gv.LEVEL-100)
        elif cur==1 and          gv.LEVEL>= 10:gv.SETLEVEL(gv.LEVEL- 10)
        elif cur==2 and          gv.LEVEL>=  1:gv.SETLEVEL(gv.LEVEL-  1)
        playse('select' if prevlevel!=gv.LEVEL else 'void')
    drawbg(5)
    lr=True if gv.XSIZE<=480 else False
    
    printg("SELECT" if lr else "LEVEL SELECT",(gv.XSIZE/2,gv.YSIZE/6),2,1,a=4,t=1)
    
    printg(str(gv.LEVEL//100%10),(gv.XSIZE/6*2,gv.YSIZE/6*3),1,1 if (cur==0) else 0,printst,a=4)
    printg(str(gv.LEVEL//10 %10),(gv.XSIZE/6*3,gv.YSIZE/6*3),1,1 if (cur==1) else 0,printst,a=4)
    printg(str(gv.LEVEL     %10),(gv.XSIZE/6*4,gv.YSIZE/6*3),1,1 if (cur==2) else 0,printst,a=4)
    
    printg("↑",(gv.XSIZE/6*2,gv.YSIZE/6*2),2 if (cur==0 and maxlevel-gv.LEVEL>=100) else 0,a=4)
    printg("↑",(gv.XSIZE/6*3,gv.YSIZE/6*2),2 if (cur==1 and maxlevel-gv.LEVEL>= 10) else 0,a=4)
    printg("↑",(gv.XSIZE/6*4,gv.YSIZE/6*2),2 if (cur==2 and maxlevel-gv.LEVEL>=  1) else 0,a=4)
    printg("↓",(gv.XSIZE/6*2,gv.YSIZE/6*4),2 if (cur==0 and          gv.LEVEL>=100) else 0,a=4)
    printg("↓",(gv.XSIZE/6*3,gv.YSIZE/6*4),2 if (cur==1 and          gv.LEVEL>= 10) else 0,a=4)
    printg("↓",(gv.XSIZE/6*4,gv.YSIZE/6*4),2 if (cur==2 and          gv.LEVEL>=  1) else 0,a=4)
    
    printg("TITLE①" if lr else "TITLE",(gv.XSIZE/2,gv.YSIZE/6*5), 1 if cur==3 else 0,3 if cur==3 else 0,printst,a=4,t=1)
    
    fadein(starttick)
    if(endtick):
        fadeout(endtick)
        if gettick()>endtick+500: gv.SETSCENE(nextscene)
