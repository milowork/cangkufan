################################
##    CANGKUFAN  FUNCTIONS    ##
##   COPYLEFT 2019 MILOWORK   ##
################################

import pygame
import math
import gv

# 字符串，位置，颜色，效果，开始时间，对齐，翻译
def printg(s:str,p:tuple,co=1,e=0,st=0,a=0,t=0):
    if(t):s=tran(s)
    x=round(p[0]); y=round(p[1])
    s2=[]
    i=0
    # 转码
    for c in s:
        if(ord(c)>=32 and ord(c)<127): # ASCII
            s2.append(ord(c)-32)
        elif(ord(c)>=127):
            if  (c=='ā'):s2.append(0x60)            # 汉语拼音和罗马音字母
            elif(c=='á'):s2.append(0x61)
            elif(c=='ǎ'):s2.append(0x62)
            elif(c=='à'):s2.append(0x63)
            elif(c=='ō'):s2.append(0x64)
            elif(c=='ó'):s2.append(0x65)
            elif(c=='ǒ'):s2.append(0x66)
            elif(c=='ò'):s2.append(0x67)
            elif(c=='ē'):s2.append(0x68)
            elif(c=='é'):s2.append(0x69)
            elif(c=='ě'):s2.append(0x6A)
            elif(c=='è'):s2.append(0x6B)
            elif(c=='ī'):s2.append(0x6C)
            elif(c=='í'):s2.append(0x6D)
            elif(c=='ǐ'):s2.append(0x6E)
            elif(c=='ì'):s2.append(0x6F)
            elif(c=='ū'):s2.append(0x70)
            elif(c=='ú'):s2.append(0x71)
            elif(c=='ǔ'):s2.append(0x72)
            elif(c=='ù'):s2.append(0x73)
            elif(c=='ǖ'):s2.append(0x74)
            elif(c=='ǘ'):s2.append(0x75)
            elif(c=='ǚ'):s2.append(0x76)
            elif(c=='ǜ'):s2.append(0x77)
            elif(c=='ü'):s2.append(0x78)
            elif(c=='ê'):s2.append(0x79)
            elif(c=='©'):s2.append(0x7A)            # COPYLEFT 符号
            elif(c=='¥' or c=='￥'):s2.append(0x7B)    # 人民币·日元符号
            elif(c=='↑'):s2.append(0x7C)                # 箭头
            elif(c=='↓'):s2.append(0x7D)
            elif(c=='←'):s2.append(0x7E)
            elif(c=='→'):s2.append(0x7F)
            elif(c=='■'):s2.append(0x5F)            # 方块
            elif(c>='①' and c<='⑩'):pass            # ①②③④⑤⑥⑦⑧⑨⑩，用于区分含义不同的字符串，不予显示
            else:         s2.append(ord(c))            # 不支持的字符
        else:
            pass        # 直接忽略
        i+=1
    # 位置处理
    if(a% 3==1):x=x-len(s2)*gv.SIZE/2    # 水平中对齐
    if(a% 3==2):x=x-len(s2)*gv.SIZE        # 水平右对齐
    if(a//3==1):y=y-gv.SIZE/2            # 垂直中对齐
    if(a//3==2):y=y-gv.SIZE                # 垂直下对齐
    # 确认参数合乎范围
    if(co<0):co=0
    if(co>3):co=3
    # TICK
    tick=max(gettick()-st,0)
    # 开始处理
    i=0
    for c in s2:
        if   e==1:o=math.sin( (tick/1000+i/5)*math.pi*2)*-8        # 波动效果
        elif e==2:o=-8 if math.floor(tick/200)%len(s2)==i else 0    # 跳动效果
        #elif e==3:o=-8 if math.floor(tick/100)==i else 0            # 跳动效果(一次性)
        elif e==3: o=(2.25-abs(tick/80-i-1.5)**2)/2.25*-8 if 0<=tick//80-i<=2 else 0 # 新·跳动效果(一次性)
        else:o=0                                                    # 静态 
        # 显示
        if   c==0 : pass
        elif c<128: gv.SCREEN.blit(gv.FONT[c+co*128],(x,y+o))
        else: gv.SCREEN.blit(gv.XFONTRENDER(c,co),(x,y+o))
        x+=gv.SIZE; i+=1

# 显示信息
def dispmsg(s,st:int,dur=2000):
    #32...32...-32
    tick=gettick()-st
    if   tick<    320 :y=(tick/5)-32
    elif tick<dur     :y=32
    elif tick<dur+320 :y=32-((tick-dur)/5)
    else: return
    printg(s,(gv.XSIZE/2,y),2,a=1)

def fadein(st:int): # 自 ST 开始淡入，逐渐变亮
    tick=gettick()-st
    if (not tick) or tick<=0:
        gv.SCREEN.fill(pygame.Color(0,0,0,0))
    elif tick<255:
        a=pygame.Surface(gv.SCREENSIZE)
        a.set_alpha(255-tick)
        gv.SCREEN.blit(a,(0,0))

def fadeout(et): # 自 ET 开始淡出，逐渐变暗
    tick=gettick()-et
    if (not et) or et<=0: pass
    elif (not tick) or tick<=0: pass
    elif tick<255:
        a=pygame.Surface(gv.SCREENSIZE)
        a.set_alpha(tick)
        gv.SCREEN.blit(a,(0,0))
    else:
        gv.SCREEN.fill(pygame.Color(0,0,0,0))

def tran(s:str):
    return gv.DICT.get(s,s)

def drawbg(n:int):
    tick=gettick()
    for     h in range(0,math.ceil(gv.YSIZE/384)  ):
        for i in range(0,math.ceil(gv.XSIZE/256)+1):
            gv.SCREEN.blit(gv.BG[n],(i*256-(tick/10%256),h*384))

def playse(se:str):
    if gv.SE==None: return
    s=gv.SE.get(se,None)
    if s!=None: s.play()

def gettick():                         # 获取当前TICK
    return pygame.time.get_ticks()
    
def playbgm(bgm:str,restart=False):        # 播放 BGM
    if gv.SE==None: return 
    if bgm=="":
        pygame.mixer.music.stop()
    elif bgm!=gv.BGM:
        pygame.mixer.music.load(gv.MYPATH+"bgm/"+bgm+".ogg")
        pygame.mixer.music.play(-1)
    elif restart:
        pygame.mixer.music.play(-1)
    gv.SETBGM(bgm)
def fadebgm(t=500):
    if gv.SE==None: return 
    pygame.mixer.music.fadeout(t)
    gv.SETBGM("")
def stopbgm(): playbgm("")            # 停止 BGM
def pausebgm(op=0):                    # 暂停 BGM，再次调用继续，OP1只暂停，OP2只继续
    if gv.SE==None: return 
    if gv.BGM=="": pass                # 无 BGM
    elif gv.BGM[0]=="/" and op!=1:    # 继续
        gv.SETBGM(gv.BGM[1:]); pygame.mixer.music.unpause()
    elif op!=2:                        # 暂停
        gv.SETBGM("/"+gv.BGM); pygame.mixer.music.pause()

def msgbox(st, info=False):
    import os, platform
    if platform.system()=='Windows':
        os.popen('''mshta vbscript:CreateObject("Wscript.Shell").popup("'''+st.replace('"','""').replace('\n','"+chr(10)+"')+'''",0,"Cangkufan",48)(window.close)''')
    else: print(st)
