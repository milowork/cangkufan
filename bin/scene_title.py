################################
##   CANGKUFAN  SCENE_TITLE   ##
##   COPYLEFT 2019 MILOWORK   ##
################################

# 导入
import pygame
import sys
import gv
from func import *
import scene_select, scene_xsb, scene_config

global starttick, endtick, nextscene, rtte # rtte = Restart To Take Effect
global cur, printst

def setup(startup=0):# 返回、启动、重启生效、保存失败
    global starttick, endtick, cur, printst, rtte
    rtte=startup
    starttick=gettick()
    endtick=0; cur=0; printst=0
    playbgm('-1')

def loop():
    global starttick, endtick, nextscene, rtte
    global cur, printst
    
    tick=gettick()
    gv.SCREEN.fill(pygame.Color(32,32,32,0))
    
    if endtick: pass
    elif gv.KEY1[pygame.K_x]:
        playse('ok'); endtick=tick; fadebgm()
        if   cur==0:    nextscene=scene_select # 开始
        elif cur==1:    nextscene=scene_xsb    # XSB
        elif cur==2:    nextscene=scene_config # 设置
        elif cur==3:    nextscene=None         # 退出
    elif gv.KEY1[pygame.K_DOWN] and cur<3: playse('select'); cur+=1; printst=tick
    elif gv.KEY1[pygame.K_UP]   and cur>0: playse('select'); cur-=1; printst=tick
    
    gv.SCREEN.blit(gv.UI["title" ], (gv.XSIZE//2-320,            0))
    gv.SCREEN.blit(gv.UI["title2"], (              0, gv.YSIZE-128))
    yoff=-32
    
    if   rtte==2: dispmsg(tran("RESTART TO TAKE EFFECT"), starttick)
    elif rtte==3: dispmsg(tran("CONFIG SAVE FAILED")    , starttick)
    
    printg("START",         (gv.XSIZE/2,gv.YSIZE/8*4+yoff),1 if cur==0 else 0,3 if cur==0 else 0,printst,a=4,t=1)
    printg("LOAD XSB",      (gv.XSIZE/2,gv.YSIZE/8*5+yoff),1 if cur==1 else 0,3 if cur==1 else 0,printst,a=4,t=1)
    printg("CONFIG",        (gv.XSIZE/2,gv.YSIZE/8*6+yoff),1 if cur==2 else 0,3 if cur==2 else 0,printst,a=4,t=1)
    printg("EXIT",          (gv.XSIZE/2,gv.YSIZE/8*7+yoff),1 if cur==3 else 0,3 if cur==3 else 0,printst,a=4,t=1)
    gv.SCREEN.blit(gv.CHAR[   2],(gv.XSIZE/2-160-gv.SIZE/2,gv.YSIZE/8*(cur+4)+yoff-gv.SIZE/2))
    gv.SCREEN.blit(gv.CHAR[ 8+1],(gv.XSIZE/2+160-gv.SIZE/2,gv.YSIZE/8*(cur+4)+yoff-gv.SIZE/2))
    if gv.NEWEST:
        gv.SCREEN.blit(gv.XFONT.render(tran('NEWEST')+': '+gv.NEWEST,1,(255,255,255)),(0,0))
    
    fadein(starttick)
    if(endtick):
        fadeout(endtick)
        if gettick()>endtick+500: gv.SETSCENE(nextscene)

