################################
##   CANGKUFAN SCENE_CONFIG   ##
##   COPYLEFT 2019 MILOWORK   ##
################################

# 导入
import pygame
import sys, csv
import gv
from func import *
import scene_title

global starttick, endtick, nextscene
global cur, printst, printst_lan, printst_reso, changed # 光标, PRINTC 开始时刻, .. , .. , 设置有所更变
global resos

def setup():
    global starttick, endtick
    global cur, printst, printst_lan, printst_reso, changed, resos
    starttick=gettick(); endtick=0
    cur=0; printst=0; printst_lan=0; printst_reso=0; changed=0
    resos=[6400360,8000480,9600540,12800720,16000900,19201080,0,-6400480,-8000600,-10240768,-12800720,-12800800,-19201080]
    playbgm("0")

def loop():
    global starttick, endtick, nextscene
    global cur, printst, printst_lan, printst_reso, changed, resos
    
    t=gettick()
    gv.SCREEN.fill(pygame.Color(0,0,0,0))

    if endtick:
        pass
    elif gv.KEY1[pygame.K_x]:
        if cur==5:
            nextscene=scene_title
            endtick=pygame.time.get_ticks()
            try: saveconfig() if changed else None
            except Exception: changed|=4
            playse('ok'); fadebgm()
    elif gv.KEY1[pygame.K_z]:
        nextscene=scene_title
        endtick=pygame.time.get_ticks()
        try: saveconfig() if changed else None
        except Exception: changed|=4
        playse('back'); fadebgm()
    elif gv.KEY1[pygame.K_DOWN]  and cur<5 :
        cur+=1
        printst=pygame.time.get_ticks()
        playse('select')
    elif gv.KEY1[pygame.K_UP]   and cur>0 :
        cur-=1
        printst=pygame.time.get_ticks()
        playse('select')
    elif gv.KEY1[pygame.K_LEFT]    and cur<=4:
        if   cur==0: gv.SETVOL(bgm=gv.BGMVOL-1); playse('grab'); changed|=1
        elif cur==1: gv.SETVOL(se =gv.SEVOL -1); playse('grab'); changed|=1
        elif cur==2:
            gv.SETLANG( gv.LANGUAGE-1); playse('grab'); changed|=1
            printst_lan=t
        elif cur==3:
            try:gv.SETRESOLUTION(resos[(resos.index(gv.RESOLUTION)-1)%len(resos)])
            except ValueError: gv.SETRESOLUTION(8000480) # 找不到则重置
            playse('grab'); changed|=3; printst_reso=t
        elif cur==4: playse('grab'); changed|=1; gv.ZXXCH^=1
    elif gv.KEY1[pygame.K_RIGHT]   and cur<=4:
        if   cur==0: gv.SETVOL(bgm=gv.BGMVOL+1); playse('grab'); changed|=1
        elif cur==1: gv.SETVOL(se =gv.SEVOL +1); playse('grab'); changed|=1
        elif cur==2:
            gv.SETLANG( gv.LANGUAGE+1); playse('grab'); changed|=1
            printst_lan=t
        elif cur==3:
            try:gv.SETRESOLUTION(resos[(resos.index(gv.RESOLUTION)+1)%len(resos)])
            except ValueError: gv.SETRESOLUTION(8000480) # 找不到则重置
            playse('grab'); changed|=3; printst_reso=t
        elif cur==4: playse('grab'); changed|=1; gv.ZXXCH^=1
    drawbg(7)
    
    lr=True if gv.XSIZE<=480 else False # 低分辨率？
    printg("CONFIG"                     ,(gv.XSIZE/2 ,gv.YSIZE/8  ),2,0 if lr else 1,a=4,t=1)
    printg("BGM"                        ,(gv.XSIZE/16,gv.YSIZE/8*2),1 if cur==0 else 0,3 if cur==0 else 0,printst,a=3,t=0)
    printg("SE"                         ,(gv.XSIZE/16,gv.YSIZE/8*3),1 if cur==1 else 0,3 if cur==1 else 0,printst,a=3,t=0)
    printg("LANG" if lr else "LANGUAGE" ,(gv.XSIZE/16,gv.YSIZE/8*4),1 if cur==2 else 0,3 if cur==2 else 0,printst,a=3,t=0)
    if not lr:printg(       "RESOLUTION",(gv.XSIZE/16,gv.YSIZE/8*5),1 if cur==3 else 0,3 if cur==3 else 0,printst,a=3,t=1)
    printg("■" *    gv.BGMVOL           ,(gv.XSIZE/16*15-(10-gv.BGMVOL)*gv.SIZE ,gv.YSIZE/8*2),1,a=5)
    printg("■" *    gv.SEVOL            ,(gv.XSIZE/16*15-(10-gv.SEVOL )*gv.SIZE ,gv.YSIZE/8*3),1,a=5)
    printg("_" *(10-gv.BGMVOL)          ,(gv.XSIZE/16*15                        ,gv.YSIZE/8*2),0,a=5)
    printg("_" *(10-gv.SEVOL )          ,(gv.XSIZE/16*15                        ,gv.YSIZE/8*3),0,a=5)
    
    if   gv.RESOLUTION==    0:r="FULLSCREEN"                                                                ; rc=2
    elif gv.RESOLUTION<-10000:r=tran("FS")+" "+str((-gv.RESOLUTION)//10000)+'*'+str((-gv.RESOLUTION)%10000) ; rc=3
    else                     :r=               str(  gv.RESOLUTION //10000)+'*'+str(  gv.RESOLUTION %10000) ; rc=1
    
    printg("ENGLISH",(gv.XSIZE/16*15,gv.YSIZE/8*4),1                  ,3,printst_lan  ,a=5,t=1)
    if lr:printg(r,(gv.XSIZE/2    ,gv.YSIZE/8*5),rc if cur==3 else 0,3, max(printst if cur==3 else 0,  printst_reso),a=4,t=1)
    else: printg(r,(gv.XSIZE/16*15,gv.YSIZE/8*5),rc                 ,3,printst_reso ,a=5,t=1)

    printg("CONFIRM KEY"           ,(gv.XSIZE/16   , gv.YSIZE/8*6),1 if cur==4 else 0, 3 if cur==4 else 0, printst, a=3, t=1)
    printg("Z" if gv.ZXXCH else "X",(gv.XSIZE/16*15, gv.YSIZE/8*6),1,                                               a=5)
    printg("TITLE"                 ,(gv.XSIZE/2    , gv.YSIZE/8*7),1 if cur==5 else 0, 3 if cur==5 else 0, printst ,a=4, t=1)
    
    fadein(starttick)
    if endtick:
        fadeout(endtick)
        if gettick()>endtick+500:
            gv.SETSCENE(nextscene,3 if changed&4 else (2 if changed&2 else 0))

def saveconfig():
    with open(gv.MYPATH+"config.ini",'w',encoding="utf-8") as f:
        csv.writer(f).writerow([gv.BGMVOL, gv.SEVOL, gv.LANGUAGE, gv.RESOLUTION, gv.ZXXCH])
