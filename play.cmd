@echo off
if "%1" == "h" goto begin
mshta vbscript:createobject("Wscript.shell").run("""%~nx0"" h",0)(window.close)&&exit
:begin
ver | find "10.0." > NUL && goto NT6
ver | find "6.3." > NUL && goto NT6
ver | find "6.2." > NUL && goto NT6
ver | find "6.1." > NUL && goto NT6
ver | find "6.0." > NUL && goto NT6
ver | find "5.2." > NUL && goto NT5
ver | find "5.1." > NUL && goto NT5
mshta vbscript:CreateObject("Wscript.Shell").popup("Windows 2000/NT4/9X is not supported.",0,"Cangkufan",48)(window.close)
exit

:NT6
if exist "%cd%\python-3.7.4-embed-win32\pythonw.exe" goto PY37E
if exist "%cd%\..\python-3.7.4-embed-win32\pythonw.exe" goto PY37E2
:NT5
if exist "C:\Python34\python.exe" goto PY34
mshta vbscript:CreateObject("Wscript.Shell").popup("Please install pygame integrated package at https://milo.work/w/pygame",0,"Cangkufan",48)(window.close)
rem explorer "https://milo.work/w/pygame"
exit

:PY37E
"%cd%\python-3.7.4-embed-win32\pythonw.exe" "%cd%\main.py"
exit
:PY37E2
"%cd%\..\python-3.7.4-embed-win32\pythonw.exe" "%cd%\main.py"
exit
:PY34
"C:\Python34\python.exe" "%cd%\main.py"
exit