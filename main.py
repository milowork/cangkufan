# coding: utf-8
################################
##       CANGKUFAN 2.30       ##
##   COPYLEFT 2019 MILOWORK   ##
################################

import sys, os, platform, traceback
win=platform.system()=='Windows'

def msgbox(st):
    if win: os.popen('''mshta vbscript:CreateObject("Wscript.Shell").popup("'''+st.replace('"','""').replace('\n','"+chr(10)+"')+'''",0,"Cangkufan",48)(window.close)''')
    else: print(st)

if __name__=='__main__':
    if sys.version_info<(3,4,0):
        msgbox("""本程序需要 Python 3.4 或以上版本。
This program need Python 3.4 or above."""+"""
请到 https://milo.work/w/pygame 下载并安装 pygame 整合包。
Please install pygame integrated package at https://milo.work/w/pygame""" if win else "")
        sys.exit(1)
    try: ################################
        import pygame
        if pygame.version.vernum<(1,9,1): raise ImportError
    except ImportError:
        if win: msgbox("""本程序需要 Pygame 1.9.1 或以上版本。
This program need pygame 1.9.1 or above.
请到 https://milo.work/w/pygame 下载并安装 pygame 整合包。
Please install pygame integrated package at https://milo.work/w/pygame""")
        else: msgbox("""请在 Shell 中输入「python3 -m pip install -U pygame」来安装或更新 pygame。
Please input "python3 -m pip install -U pygame" in shell to install or update pygame.""")
        sys.exit(1)
    try: ################################
        sys.path.append(os.getcwd()+"/bin/")
        import main2
        main2.setup()
    except Exception:
        msgbox(traceback.format_exc()); sys.exit(1)